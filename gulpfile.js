var gulp = require('gulp');
var rm   = require('gulp-rm');
var sass = require('gulp-sass');
var sMap = require('gulp-sourcemaps');
var sync = require('browser-sync');

var webpack  = require('webpack');
var gWebpack = require('webpack-stream');



var swallowError = function (e) {
  this.emit('end')
};

gulp.task('webmock', [
  'build',
  'sync-webmock'
], () => {
  gulp.watch('./remote/index.html', ['reload']);
  gulp.watch('./dist/*', ['reload']);
});

gulp.task('default', [
  'build',
  'sync'
], () => {
  gulp.watch('./src/resources/**/*', ['copy-resources-wrapper']);
  gulp.watch('./src/app/stylesheets/**/*.scss', ['build-stylesheets-wrapper']);
  gulp.watch(['./src/**/*', '!./src/app/stylesheets/**/*'], ['build-ts-wrapper']);
});

gulp.task('build-prod', [
  'build-ts-prod',
  'build-stylesheets',
  'copy-resources'
]);

gulp.task('build', [
  'build-ts',
  'build-stylesheets',
  'copy-resources'
], () => {
  sync.reload();
});

gulp.task('reload', () => {
  sync.reload();
});
gulp.task('sync', () => {
  sync.init({
    server : {
      baseDir : "./dist"
    }
  });
});
gulp.task('sync-webmock', () => {
  sync.init({
    server : {
      baseDir : "./remote",
      index: 'index.html'
    }
  });
});


gulp.task('clean-ts', () => {
  return gulp.src(['dist/bundle.js'], {read : false})
    .pipe(rm());
});
gulp.task('build-ts', ['clean-ts'], () => {
  return gulp.src('./src/app/index.ts')
    .pipe(gWebpack(require('./webpack/webpack.dev.js'), webpack))
    .on('error', swallowError)
    .pipe(gulp.dest('./dist'));
});
gulp.task('build-ts-prod', ['clean-ts'], () => {
  return gulp.src('./src/app/index.ts')
    .pipe(gWebpack(require('./webpack/webpack.prod.js'), webpack))
    .pipe(gulp.dest('./dist'));
});
gulp.task('build-ts-wrapper', ['build-ts'], () => {
  sync.reload();
});


gulp.task('clean-resources', () => {
  return gulp.src(['./dist/**/*!*.js'], {read : false})
    .pipe(rm());
});
gulp.task('copy-resources', ['clean-resources'], () => {
  return gulp.src(['./src/resources/**/*'])
    .pipe(gulp.dest('./dist'));
});
gulp.task('copy-resources-wrapper', ['copy-resources'], function () {
  sync.reload();
});



gulp.task('clean-stylesheets', () => {
  return gulp.src('./dist/main.css', {read : false})
    .pipe(rm());
});
gulp.task('build-stylesheets', ['clean-stylesheets'], () => {
  return gulp.src('./src/app/stylesheets/main.scss')
    .pipe(sMap.init())
    .pipe(sass({outputStyle : 'compressed'}).on('error', sass.logError))
    .pipe(sMap.write())
    .pipe(gulp.dest('./dist'));
});
gulp.task('build-stylesheets-wrapper', ['build-stylesheets'], () => {
  sync.reload();
});
