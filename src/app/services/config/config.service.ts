import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

export interface IConfigState {
  siteOffline: boolean,
}

export interface IMutateConfigState {
  siteOffline: boolean,
}

const ConfigRecord: IConfigState = {
  siteOffline: false,
};

@Injectable()
export class ConfigService {

  public state: BehaviorSubject<IConfigState> = new BehaviorSubject<IConfigState>(ConfigRecord);

  private urlConfig: any = {};

  constructor(
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
  ) {
    this.loadFromURI();
    this.loadFromFile();
  }

  public mutate(newState: IMutateConfigState) {
    let oldState = this.state.value;

    for (let key in newState) {
      if (oldState.hasOwnProperty(key)) {
        oldState[key] = newState[key];
      }
    }

    this.state.next(oldState);
  }

  private loadFromFile() {
    this.http.get('./config.json').subscribe((data) => {
      let pushConfig: IConfigState = <IConfigState>{};

      if (data) {
        for (let key in data) {
          if (key in ConfigRecord) {
            pushConfig[key] = data[key];
          }
        }

        if (pushConfig) {
          this.mutate(pushConfig);
        }
      }

      if (this.urlConfig) {
        this.mutate(this.urlConfig);
      }
    });
  }

  private loadFromURI() {
    this.urlConfig = <IConfigState>{};

    this.activatedRoute.queryParams.subscribe((params) => {
      if (params) {
        for (let key in params) {
          if (key.indexOf('config-') === 0) {
            let identifier = key.replace(/config-/, '');

            if (identifier in ConfigRecord) {
              this.urlConfig[identifier] = eval(params[key]);
            }
          }
        }

        if (this.urlConfig) {
          this.mutate(this.urlConfig);
        }
      }
    });
  }
}
