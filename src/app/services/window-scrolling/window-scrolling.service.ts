import { Injectable } from '@angular/core';

@Injectable()
export class WindowScrollingService {

  private styleTag: HTMLStyleElement;

  constructor() {
    this.styleTag = WindowScrollingService.buildStyleElement();
  }

  public disbale(): void {
    document.body.appendChild(this.styleTag);
  }

  public enable(): void {
    if(document.getElementById('window-scrolling-service')){
      document.body.removeChild(this.styleTag);



    }
  }

  private static buildStyleElement(): HTMLStyleElement {
    const style = document.createElement('style');

    style.type = 'text/css';
    style.id = 'window-scrolling-service';
    style.textContent = 'body{overflow:hidden!important;}';

    return style;
  }

}
