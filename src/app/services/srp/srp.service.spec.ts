import { TestBed, inject } from '@angular/core/testing';

import { SrpService } from './srp.service';

describe('SrpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SrpService]
    });
  });

  it('should be created', inject([SrpService], (service: SrpService) => {
    expect(service).toBeTruthy();
  }));
});
