import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

export interface ISrpEvent {
  start: Date,
  end: Date,
  title: string,
  women: boolean,
  icon: string
}

export interface ISrpState {
  events: ISrpEvent[]
}

const SrpRecord: ISrpState = {
  events: null,
};

@Injectable()
export class SrpService {

  public state: BehaviorSubject<ISrpState> = new BehaviorSubject<ISrpState>(SrpRecord);

  constructor(
    private http: HttpClient,
  ) {
    this.loadFromFile();
  }

  private loadFromFile() {
    this.http.get('./srp.json').subscribe((data: any[]) => {

      if (data) {
        this.state.next({
          events: data.map((event) => {
            return {
              start: new Date(event.start),
              end: event.end !== null ? new Date(event.end) : null,
              title: event.title,
              women: event.woman,
              icon: event.icon !== null ? event.icon : ''
            };
          }),
        });
        this.state.complete();
      }
    });
  }
}
