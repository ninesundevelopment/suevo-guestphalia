import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar/sidebar.service';
import { ScrollService } from '../../services/scroll/scroll.service';
import { ConfigService, IConfigState } from '../../services/config/config.service';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'taw-base',
  template: require('./app.component.html'),
  styles: [require('./app.component.scss')],
})
export class AppComponent implements OnInit {

  public blockerIsVisible     = true;
  public sidebarVisible       = false;
  public config: IConfigState = <IConfigState>{};

  constructor(
    private sidebarSrv: SidebarService,
    private scrollSrv: ScrollService,
    private configSrv: ConfigService,
    private router: Router,
  ) {
    this.configSrv.state.subscribe((state) => {
      if (this.config !== state)
        this.config = state;

      if (this.config.siteOffline)
        this.router.navigateByUrl('/offline').then();
    });

    this.router.events.subscribe((event) => {
      if (this.config.siteOffline && event instanceof NavigationStart) {
        if (event.url !== '/offline')
          this.router.navigateByUrl('/offline').then();
      }
    });

    this.sidebarSrv.visible.subscribe((value) => {
      if (!value) window.setTimeout(() => {
        this.sidebarVisible = false
      }, 300);
      else this.sidebarVisible = true;
    });

    this.scrollSrv.scrollTop.subscribe((value) => {
      document.querySelector('body').scrollTop = value;
    });
  }

  ngOnInit() {
    window.setTimeout(() => {
      this.blockerIsVisible = false;

      if (this.config.siteOffline){
        this.router.navigateByUrl('/offline').then();
      }
    }, 500);
  }
}
