import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar/sidebar.service';
import { INavigation, Navigation } from '../../app-routing.module';
import { NavigationStart, Router } from '@angular/router';
import { WindowScrollingService } from '../../services/window-scrolling/window-scrolling.service';

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public visible: boolean = false;
  public routes: INavigation[];

  constructor(
    public sidebarSrv: SidebarService,
    private router: Router,
    private wss: WindowScrollingService,
  ) {
    this.sidebarSrv.visible.subscribe((value) => {
      this.visible = value;

      if (this.visible) this.wss.disbale();
      else this.wss.enable();
    });

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.sidebarSrv.visible.next(false);
      }
    });
  }

  ngOnInit() {
    this.routes = Navigation.filter((route) => {
      return route.navigation;
    })
  }

  public closeSidebar() {
    this.sidebarSrv.visible.next(false);
  }

}
