import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar/sidebar.service';
import { INavigation, Navigation } from '../../app-routing.module';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public sidebarVisible = false;
  public routes: INavigation[];

  constructor(
    private sidebarSrv: SidebarService
  ) { }

  ngOnInit() {
    this.routes = Navigation.filter((route: INavigation) => route.navigation);
  }

  public toggleSidebar() {
    this.sidebarSrv.visible.next(!this.sidebarSrv.visible.value)
  }
}
