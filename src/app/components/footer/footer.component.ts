import { Component, OnInit } from '@angular/core';
import { INavigation } from '../../app-routing.module';
import { ScrollService } from '../../services/scroll/scroll.service';
import { IPackage } from '../../interfaces/package';

declare const PACKAGE_JSON: IPackage;

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {

  public version: string = PACKAGE_JSON.version;
  public author: string = PACKAGE_JSON.author;

  public routes: INavigation[] = [
    {
      name: 'Zum Seitenanfang',
      action: this.scrollTop.bind(this),
    },
    {
      name: 'Impressum',
      route: {
        path: 'imprint',
      },
    },
  ];

  constructor(
    private scrollSrv: ScrollService,
  ) {
  }

  ngOnInit() {
  }

  public scrollTop() {
    this.scrollSrv.scrollTop.next(0);
  }
}
