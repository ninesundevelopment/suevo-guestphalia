import { ISrpState, SrpService } from '../../services/srp/srp.service';
import {
  Component, Inject, LOCALE_ID, OnDestroy, OnInit,
} from '@angular/core';
import {
  isSameDay,
  isSameMonth,
  endOfDay,
} from 'date-fns';
import { Subject } from 'rxjs';
import {
  CalendarEvent, CalendarEventTitleFormatter,
  CalendarView,
} from 'angular-calendar';
import { DatePipe } from '@angular/common';

class CustomEventTitleFormatter extends CalendarEventTitleFormatter {
  constructor(@Inject(LOCALE_ID) private locale: string) {
    super();
  }

  // you can override any of the methods defined in the parent class

  month(event: CalendarEvent): string {
    return `<b>${new DatePipe(this.locale).transform(
      event.start,
      'hh:mm',
      this.locale,
    )}</b> ${event.title}`;
  }

  week(event: CalendarEvent): string {
    return `<b>${new DatePipe(this.locale).transform(
      event.start,
      'hh:mm',
      this.locale,
    )}</b> ${event.title}`;
  }

  day(event: CalendarEvent): string {
    return `<b>${new DatePipe(this.locale).transform(
      event.start,
      'hh:mm',
      this.locale,
    )}</b> ${event.title}`;
  }
}

@Component({
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter,
    },
    SrpService
  ],
})
export class EventsComponent implements OnInit, OnDestroy {

  locale: string           = 'de';
  view: CalendarView       = CalendarView.Month;
  CalendarView             = CalendarView;
  viewDate: Date           = new Date();
  refresh: Subject<any>    = new Subject();
  events: CalendarEvent[]  = null;
  activeDayIsOpen: boolean = false;
  state: ISrpState         = null;
  subscriber               = null;

  constructor(
    private ss: SrpService,
  ) {
  }

  ngOnInit() {
    this.subscriber = this.ss.state.subscribe((state) => {
      if (state && state.events !== null) {
        this.state = state;
        this.updateEvents();
      }
    });
  }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
  }

  dayClicked({date, events}: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (isSameDay(this.viewDate, date) && this.activeDayIsOpen) {
        this.activeDayIsOpen = false;
      } else {
        this.viewDate        = date;
        this.activeDayIsOpen = isSameDay(this.viewDate, date) && events.length > 0;
      }
    }
  }

  isThisMonth(day): boolean {
    return day.inMonth;
  }

  updateEvents() {
    this.events = this.state.events.map((event) => {
      return {
        start: event.start,
        end: event.end == null ? endOfDay(event.start) : event.end,
        title: event.title,
        color: {
          primary: '#00b300',
          secondary: '#006f00',
        },
        actions: null,
        allDay: false,
        draggable: false,
        meta: {
          incrementsBadgeTotal: false,
          women: event.women,
          icon: event.icon,
        },
      };
    });
  }
}
