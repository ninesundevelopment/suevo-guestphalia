import { Component, HostListener } from '@angular/core';
import { ISrpEvent, ISrpState, SrpService } from '../../services/srp/srp.service';

@Component({
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent {

  public semester: string = '';
  public nextSemester: string = '';
  public facebookURI: string = 'https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FSuevoGuestphalia%2F&tabs=timeline&width=320&height=500&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId';
  public deviceWidth: number = 320;

  public nextEvents: ISrpEvent[] = [];
  private srpState: ISrpState = null;


  constructor(
    private srpService: SrpService,
  ) {
    this.srpService.state.subscribe((state) => {
      this.srpState = state;
      this.getNextEvents();
      console.log(this.nextEvents);
    });

    this.buildFacebookURI();

    const currentDate = new Date();
    this.semester = this.buildSemesterString(currentDate);
    this.nextSemester = this.buildSemesterString(new Date(
      currentDate.getFullYear(),
      currentDate.getMonth() + 6,
      currentDate.getDate()
    ));
  }
  
  buildSemesterString(date) {
    if (date.getMonth() >= 3 && date.getMonth() < 6) {
      // Sommersemester: April(3) - Juli(6)
      return 'Sommersemester ' + date.getFullYear();
    } else {
      // Wintersemester
      if (date.getMonth() < 3) {
        return 'Wintersemester ' + (date.getFullYear() - 1) + ' / ' + date.getFullYear();
      } else {
        return 'Wintersemester ' + date.getFullYear() + ' / ' + (date.getFullYear() + 1);
      }
    }
  }

  getNextEvents() {
    if (!this.srpState.events) return;
    const currentDate = new Date();
    const futureEvents = this.srpState.events.filter(
      (event) => {
        return event.start > currentDate &&
          event.title !== 'CC' &&
          event.title !== 'ZC' &&
          (event.start.getTime() - currentDate.getTime()) / 86400000 <= 31;
      });
    this.nextEvents = futureEvents;
  }


  @HostListener('window:resize', [])
  onResize() {
    this.buildFacebookURI();
  }

  private buildFacebookURI() {
    let newWidth = 0;

    if (window.innerWidth < 901) {
      newWidth = window.innerWidth * 0.9;
    } else {
      newWidth = window.innerWidth * 0.45;
    }

    newWidth = Math.max(Math.min(Math.floor(newWidth), 500), 310);

    if (newWidth !== this.deviceWidth) {
      this.deviceWidth = newWidth;
      this.facebookURI = 'https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FSuevoGuestphalia%2F&tabs=timeline&width=' + this.deviceWidth + '&height=500&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId';
    }
  }

}
