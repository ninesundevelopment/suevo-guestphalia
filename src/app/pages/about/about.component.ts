import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {

  constructor() { }

  public openFB() {
    window.open('http://www.franconia-berlin.de/');
  }
  public openNH() {
    window.open('https://www.normannia-hannover.de/');
  }
  public openSG() {
    window.open('http://www.suevo-guestphalia.de/');
  }
}
