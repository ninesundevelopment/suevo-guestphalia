import { Component, OnInit } from '@angular/core';
import { IDependency, IPackage } from '../../interfaces/package';

declare const PACKAGE_JSON: IPackage;

@Component({
  selector: 'credits',
  templateUrl: './credits.component.html',
  styleUrls: ['./credits.component.scss']
})
export class CreditsComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

  getDeps(): IDependency[] {
    let deps: IDependency[] = [];

    for(let key in PACKAGE_JSON.dependencies) {
        if(PACKAGE_JSON.dependencies.hasOwnProperty(key)) {
            deps.push({
              name: key,
              version: PACKAGE_JSON.dependencies[key]
            });
        }
    }
    for(let key in PACKAGE_JSON.devDependencies) {
        if(PACKAGE_JSON.devDependencies.hasOwnProperty(key)) {
            deps.push({
              name: key,
              version: PACKAGE_JSON.devDependencies[key]
            });
        }
    }

    return deps;
  }
}
