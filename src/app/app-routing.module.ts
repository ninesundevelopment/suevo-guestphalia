import { isDevMode, NgModule } from '@angular/core';

import {
  RouterModule,
  Routes,
} from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
import { ImprintComponent } from './pages/imprint/imprint.component';
import { ContactComponent } from './pages/contact/contact.component';
import { EventsComponent } from './pages/events/events.component';
import { InfoComponent } from './pages/info/info.component';
import { AboutComponent } from './pages/about/about.component';
import { CreditsComponent } from './pages/credits/credits.component';
import { HistoryComponent } from './pages/info/history/history.component';
import { FencingComponent } from './pages/info/fencing/fencing.component';
import { LinksComponent } from './pages/info/links/links.component';
import { JoinComponent } from './pages/join/join.component';
import { OfflineComponent } from './pages/offline/offline.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

export interface INavigation {
  route?: any;
  name?: string;
  navigation?: boolean;
  url?: string;
  action?: () => void;
}

export const Navigation: INavigation[] = [
  {
    route: {
      path: '',
      pathMatch: 'full',
      redirectTo: '/home',
    },
  },
  {
    name: 'Start',
    navigation: true,
    route: {
      path: 'home',
      component: IndexComponent,
    },
  },
  {
    name: 'Über uns',
    navigation: true,
    route: {
      path: 'about',
      component: AboutComponent,
    },
  },
  {
    name: 'Informatives',
    navigation: true,
    route: {
      path: 'information',
      component: InfoComponent,
    },
  },
  {
    name: 'Geschichte',
    route: {
      path: 'information/history',
      component: HistoryComponent,
    },
  },
  {
    name: 'Fechten und Mensur',
    route: {
      path: 'information/fencing',
      component: FencingComponent,
    },
  },
  {
    name: 'Querverweise',
    route: {
      path: 'information/links',
      component: LinksComponent,
    },
  },
  {
    name: 'Veranstaltungen',
    navigation: true,
    route: {
      path: 'events',
      component: EventsComponent,
    },
  },
  {
    name: 'Kontakt',
    navigation: true,
    route: {
      path: 'contact',
      component: ContactComponent,
    },
  },
  {
    name: 'Mitglied werden',
    route: {
      path: 'join',
      component: JoinComponent,
    },
  },
  {
    name: 'Impressum',
    route: {
      path: 'imprint',
      component: ImprintComponent,
    },
  },
  {
    name: 'Credits',
    route: {
      path: 'credits',
      component: CreditsComponent,
    },
  },
  {
    name: '404',
    route: {
      path: '404',
      component: NotFoundComponent,
    },
  },
  {
    name: 'Offline',
    route: {
      path: 'offline',
      component: OfflineComponent,
    },
  },
  {
    route: {
      path: '**',
      redirectTo: '/404',
    },
  },
];

const routes: Routes = Navigation.map(x => x.route);

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: isDevMode(),
      scrollPositionRestoration: 'disabled',
      anchorScrolling: 'enabled',
      urlUpdateStrategy: 'eager',
    }),
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule {
}
